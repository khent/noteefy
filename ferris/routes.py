from ferris.core import routing, plugins

routing.auto_route()


#from ferris.controllers.root import Root
#from ferris.controllers.oauth import Oauth

#routing.add(routing.Route('/admin', Root, handler_method='admin'))
#routing.add(routing.Route('/error/<code>', Root, handler_method='error'))
#routing.route_controller(Oauth)
routing.redirect('/', to= '/ng-view')

#Plugins
#plugins.enable('settings')
#plugins.enable('oauth_manager')
plugins.enable('angular')
