
AppServices.service('NoteRest', function($http, $cookieStore, $rootScope, $timeout, $window) {
    var canvas_list = [];
    var response = {};

    return {
        accountList: function(params){
            return $http.get('/api/notefires/canvaslists', {params:params});
        },
        accountDetails: function(params){
            return $http.get('/api/accounts/account_details/'+params, {});
        },
        accountDelete: function(params){
            return $http.get('/api/accounts/delete/'+params, {params:params});
        },

        customerList: function(params){
            return $http.get('/api/users/list_users');

        },


        UserLogInProcess: function(puser,ppass){
            return $http.post('/api/users/mylogin_process/'+puser+"/"+ppass);

        },

        UserRegisterProcess: function(params){
            return $http.post('/api/users/myregister_process/'+params, {params:params});

        },




        canvasCreate:  function(params,callback){
            $http.post('api/notefires/savecanvas', params)
                .success(function(data, status, headers, config){
                      if(data.code == 200){
                          response.success = data.success;
                          response.message = data.message;
                          canvas_list.push(params);
                          console.log(canvas_list);
                          $cookieStore.put('canvas_list', canvas_list);
                      } else {
                        response.success = data.success;
                        response.message = data.message;
                      }
                      callback(response);
                  }).error(function(data, status, headers, config){
                      response.success = data.success;
                      response.message = data.message;
                      callback(response);
                });

            /*$timeout(function(){
                var response = { success:  params.title === 'test'};
                if(!response.success) {
                    response.message = 'Canvas title should be "test".';
                } else {
                    canvas_list.push(params);
                }
                callback(response);
            }, 1000);*/
        },
        canvasList: function(params,callback){
            console.log('before cookie canvas_list:');

            //$cookieStore.remove('canvas_list');

            this.canvas_list_cookies = $cookieStore.get('canvas_list') || {};
            //console.log(this.canvas_list_cookies.length + ' -> canvas_length: ' + canvas_list.length);
            if (this.canvas_list_cookies.length > 0) {
                response.success = true;
                response.data = this.canvas_list_cookies;
                canvas_list = this.canvas_list_cookies;
                callback(response);
                console.log('cookie');
            }
            else
            {
                console.log('http');
                $http.get('api/notefires/canvaslists?currentUser='+params, {})
                    .success(function(data, status, headers, config){
                        if(data.code == 200){
                          response.success = data.success;
                          canvas_list = data.data;
                          $cookieStore.put('canvas_list', canvas_list);
                          response.data = canvas_list;

                        } else {
                            response.success = data.success;
                            response.data = data.data;
                        }
                        callback(response);
                    }).error(function(data, status, headers, config){
                      response.success = data.success;
                      response.data = data.data;
                      callback(response);
                    });
            }

            /*$timeout(function(){
                var response = { success:  params.owner === 'rich'};
                if(!response.success) {
                    response.message = 'Error while fetching data.';
                    response.data = [];
                }else
                {
                    response.message = 'data is fetch.';
                    response.data = canvas_list;
                }
                callback(response);
            }, 1000);*/
        },

        canvasDelete: function(params){
            return $http.get('/api/notefires/delete/'+params, {params:params});
        },

        notesCreate: function(params){
            return $http.post('/api/notefires/savenotes', params);
        },
        notesList: function(params,creator, callback){
            $http.get('/api/notefires/noteslists/'+params+'/'+creator, {})
                .success(function(data, status, headers, config){
                      if(data.code == 200){
                          response.success = data.success;
                          response.data = data.data;

                      } else {
                        response.success = data.success;
                        response.message = data.message;
                      }
                      callback(response);
                  }).error(function(data, status, headers, config){
                      response.success = data.success;
                      response.message = data.message;
                      callback(response);
                });

        },
        notesDelete: function(params){
            return $http.get('/api/notes/delete/'+params, {params:params});
        },

        notesUpdate: function(params){
            return $http.post('api/notefires/updatenotes', params);
        },

        update: function(params){
            return this.save(params);
        },
        remove: function(params){
            return this.delete(params);
        }
    };
});
