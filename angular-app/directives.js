  AppDirectives.directive("modalCreateCanvas", function(){
    return {
     restrict: "E",
     templateUrl: "ng/templates/notes/canvas-modal.html"
    };
  });

  AppDirectives.directive("createNote", function($document, NoteRest){
    return {
     restrict: "E",
     //templateUrl : "ng/templates/notes/note.html",
     template: '<textarea id="{{id}}" cols="30" rows="5" class="notes-div" style="{{style}}">{{content}}</textarea>',
     scope: {
      id: "@noteId",
      content: "@noteContent",
      style: "@noteStyle",
      canvas_id: "@noteCanvas"
     },
     //require: 'myDraggable',
     link: function(scope, element, attr){
        var startX = 0, save = null, startY = 0, x = 0, y = 0, tElement = null, post = null, top = 0, left = 0, textarea = null;

        tElement = element.find('textarea');
        //tElement.autoResize();

        post = attr.noteStyle.split(' ');
        top = post[1].split('px;');
        left = post[3].split('px;');
        y = top[0];
        x = left[0];

        console.log('top: '+ y + '; left: '+ x);

        this.note = {
          id: '',
          canvas_id: '',
          content: '',
          style: ''
        };

        /* SAVE FUNCTION */
        //retrieve (only on page load)

        save = function() {
          console.log('Save Notes call()');
          console.log('canvas_id: ' + scope.canvas_id);
          textarea = $('#'+scope.id).val();

          if(textarea != undefined)
          {

            this.note = {
              id: scope.id,
              canvas_id: scope.canvas_id,
              content: textarea,
              style: "top: "+y+"px; left: "+x+"px;"
            };

            NoteRest.notesUpdate(this.note), function(response){
              if(!response.success){
                $scope.error = response.message;
              }
            };

            console.log('notes saved!');
          }
        }
        // retrieve (only on page load)
        /*if(window.localStorage){
          elm.value = localStorage.getItem(scope.id);
          element.css({
            top: localStorage.getItem(scope.id+'cssY') + 'px',
            left:  localStorage.getItem(scope.id+'cssX') + 'px'
          });

        }*/

        // autosave onchange and every 500ms and when you close the window
        //elm.onchange = save();
        setInterval( save, 10000);
        window.onunload = save();

        tElement.bind('keyup', function($event) {
          var element = $event.target;

          $(element).height(0);
          var height = $(element)[0].scrollHeight;

          // 8 is for the padding
          if (height < 20) {
              height = 28;
          }
          $(element).height(height-8);
        });

        // Expand the textarea as soon as it is added to the DOM
        setTimeout( function() {
            var element = tElement;

            $(element).height(0);
            var height = $(element)[0].scrollHeight;

            // 8 is for the padding
            if (height < 20) {
                height = 28;
            }
            $(element).height(height-8);
        }, 0);


        tElement.css({
         position: 'relative',
         border: '1px solid red',
         cursor: 'move'
        });

        tElement.on('dblclick', function(event) {
          event.preventDefault();
          startX = event.pageX - x;
          startY = event.pageY - y;
          $document.on('mousemove', mousemove);
          $document.on('mouseup', mouseup);
          console.log('dblclick');
          tElement.css({
            border: '1px solid blue',
            cursor: 'point',
            'z-index': 1,
            cursor: 'move'
          });
        });

      function mousemove(event) {
        y = event.pageY - startY;
        x = event.pageX - startX;
        tElement.css({
          top: y + 'px',
          left:  x + 'px',
          border: '1px solid red',
          cursor: 'move',
          'z-index': 1
        });
      }

      function mouseup() {
        $document.off('mousemove', mousemove);
        $document.off('mouseup', mouseup);
        tElement.focus();
        tElement.css({
         border: '1px solid green',
         cursor: 'text'
        });
        console.log('top: '+ y +'; left: '+ x);
      }


     }

    };
  });

  AppDirectives.directive('myDraggable', function($document,$document, NoteRest) {
    return function(scope, element, attr) {
      var startX = 0, startY = 0, x = 0, y = 0;

      element.autoResize();
      console.log(attr.id);

      this.note = {
        id: '',
        canvas_id: '',
        content: ''
      };

      //retrieve (only on page load)
      var elm = document.getElementById(attr.id);
      /* save */
      var save = function() {

        /*NoteRest.notesUpdate(this.note), function(response){
          if(!response.success){
            $scope.error = response.message;
          }
        });*/
        localStorage.removeItem(attr.id);
        console.log('save()');
        console.log(localStorage.getItem(attr.id));
        localStorage.setItem(attr.id, elm.value);
        localStorage.setItem(attr.id+'cssX',  x);
        localStorage.setItem(attr.id+'cssY',  y);
      }
      /* retrieve (only on page load) */
      if(window.localStorage){
        elm.value = localStorage.getItem(attr.id);
        element.css({
          top: localStorage.getItem(attr.id+'cssY') + 'px',
          left:  localStorage.getItem(attr.id+'cssX') + 'px'
        });

      }

      /* autosave onchange and every 500ms and when you close the window */
      elm.onchange = save();
      setInterval( save, 5000);
      window.onunload = save();

      element.css({
       position: 'relative',
       border: '1px solid red',
       cursor: 'move'
      });

      element.on('dblclick', function(event) {
        event.preventDefault();
        startX = event.pageX - x;
        startY = event.pageY - y;
        $document.on('mousemove', mousemove);
        $document.on('mouseup', mouseup);
        console.log('dblclick');
        element.css({
          border: '1px solid blue',
          cursor: 'point',
          'z-index': 1,
          cursor: 'move'
        });
      });

      /*element.on('mousedown', function(event) {
        // Prevent default dragging of selected content
        event.preventDefault();
        element.focus();
        element.css({

          border: '1px solid blue',
          cursor: 'point',
          'z-index': 1
        });
      });*/

      function mousemove(event) {
        y = event.pageY - startY;
        x = event.pageX - startX;
        element.css({
          top: y + 'px',
          left:  x + 'px',
          border: '1px solid red',
          cursor: 'move',
          'z-index': 1
        });
      }

      function mouseup() {
        $document.off('mousemove', mousemove);
        $document.off('mouseup', mouseup);
        element.focus();
        element.css({
         border: '1px solid green',
         cursor: 'text'
        });
        console.log('top: '+ y +'; left: '+ x);
      }

    };

  });
