angular.module('app', ['ui.bootstrap']);
function CarouselDemoCtrl($scope){
  $scope.myInterval = 2000;
  $scope.slides = [
    {
      image: 'a.jpg'
    },
    {
      image: 'b.jpg'
    },
    {
      image: 'c.gif'
    },
    {
      image: 'd.png'
    }
  ];
}
