

AppControllers.controller('loginController', function($scope, $rootScope, $window,$location,NoteRest){
  "use strict";
    /* Put your code here */
    $scope.account = {
      email: '',
      username: '',
      password: ''
    };

    $scope.accountLogin = {
        username: '',
        password: ''
    }
    $scope.data = {};
    $scope.customer = {}; // for customer creation
    $scope.data.customer = {};
    $scope.selected_record_key = ''

    $scope.login = function () {
        // alert('insert auth here');
        $scope.UserLogInProcess();
        // alert($scope.accountLogin.username);
        // $scope.dataLoading = true;
        // console.log($scope.accountLogin);
    };

    $scope.register = function () {
          $scope.UserRegisterProcess();
        // $scope.dataLoading = true;
        // console.log($scope.account);
        // AuthenticationService.Register($scope.account, function(response) {
        //     if(response.success) {
        //         $location.path('/login');
        //         $scope.account = {};
        //         $scope.message = response.message;
        //     } else {
        //         $scope.error = response.message;
        //     }
        //     $scope.dataLoading = false;
        // });
    };
    $scope.UserLogInProcess = function(){
        NoteRest.UserLogInProcess($scope.accountLogin.username,$scope.accountLogin.password)
            .success(function(data){
                console.log('success');
                alert('Log In Successful');
                $scope.customerList();
                //redirectTo: '/ng-view/getStarted';
                //window.location.href ='/ng-view/getStarted';
                $window.location.href = 'ng-index?'+$scope.accountLogin.username;
            }).error(function(data, status, headers, config){
                console.log('Log In Failed');
                alert('Log In Failed');
            });
    };
   $scope.UserRegisterProcess = function(){
        NoteRest.UserRegisterProcess($scope.account)
            .success(function(data, status, headers, config){
                if(status == 200){
                    alert('Account Created');
                }
                // $scope.loadList();
            }).error(function(data, status, headers, config){
                alert('Account registration failed')
                console.log('Create Account Failed');
                $scope.data.result = {};
            });
    };





    $scope.data.customerList = [];

    $scope.customerList = function(){

        NoteRest.customerList()
            .success(function(data, status, headers, config){

                console.log('LIST User Result:', data);
                $scope.data.customerList = data.items;

            }).error(function(data, status, headers, config){
                console.log('LIST User Failed');
                $scope.data.customerList = [];
            });
    };
    $scope.customerList();


});
