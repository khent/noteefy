AppControllers.controller('notesController', function($scope,$rootScope, $cookieStore, $routeParams, NoteRest, $compile){

    this.canvas_id = $routeParams.canvas_id;
    $scope.account = { currentUser: $rootScope.globals.currentUser.username }
    $scope.notes = {
      notes_id : 'notes'+Math.floor(Date.now() / 1000),
      creator  : $scope.account.currentUser,
      content  : '',
      canvas_id: this.canvas_id,
      style: "top: 79px; left: 99px;"
    };

    $scope.createNote = function(){

        var node = '<create-note note-id="'+$scope.notes.notes_id+'"';
            node += 'note-content="'+$scope.notes.content+'" note-style="'+$scope.notes.style+'" note-canvas = "'+$scope.notes.canvas_id+'" ></create-note>';
        $(".canvas").append( $compile(node)($scope) );

        console.log($scope.notes.notes_id);
        NoteRest.notesCreate(this.notes, function(response) {
            console.log(response);
            if(response.success) {
                $scope.message = response.message;
            } else {
                $scope.error = response.message;
            }
        });
    }

    $scope.showOnCanvas = false;
    if($routeParams.canvas_id != undefined)
    {
      $scope.showOnCanvas = true;
    }


  });
