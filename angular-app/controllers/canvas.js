AppControllers.controller('canvasController', function($scope,$rootScope, $cookieStore, $routeParams,$compile, NoteRest, $window){
    var notes = [], canvas_id = 0;
    $scope.note = {};
    $scope.account = { currentUser: $rootScope.globals.currentUser.username }

    this.getAllNotes = {
      canvas_id: $routeParams.canvas_id,
      creator: $scope.account.currentUser
    }

    $scope.canvas = {
      canvas_name: '',
      canvas_id: Math.floor(Date.now() / 1000),
      creator: $scope.account.currentUser,
    };

    $scope.canvasCreate = function() {
      console.log($scope.canvas);
      $scope.dataLoading = true;
      $scope.error = '';
      $scope.message = '';

      NoteRest.canvasCreate($scope.canvas, function(response) {
          //console.log(response);
          if(response.success) {
              $scope.message = response.message;
              $('#canvasModal').modal('hide');

              NoteRest.canvasList($scope.account.currentUser , function(response) {
                console.log('=== canvasList === AppControllers');
                console.log(response);
                if(response.success) {
                    $scope.links = response.data;
                } else {
                    $scope.errors = response.data;
                }
              });

          } else {
              $scope.error = response.message;
          }
          $scope.dataLoading = false;
      });
    };

    /*this.canvas_list_cookies = $cookieStore.get('canvas_list') || {};
    console.log('Canvas ID: ', $routeParams.canvas_id);

    for (var i = this.canvas_list_cookies.length - 1; i >= 0; i--) {
      canvas_id = this.canvas_list_cookies[i].canvas_id;
      notes = this.canvas_list_cookies[i].Notes;

      console.log('FOR LOOP count: ' + i);
      console.log($routeParams.canvas_id + ' == ' + canvas_id);

      if($routeParams.canvas_id == canvas_id){
        for (var prop in notes) {
          $scope.note = {
            id : notes[prop]['id'],
            content : notes[prop]['content'],
            creator : notes[prop]['creator'],
            date    : notes[prop]['date']['month']+ '/' + notes[prop]['date']['day'] + '/' + notes[prop]['date']['year'],
            style   : notes[prop]['style']

          };
          console.log(notes[prop]['content']);

          var node =  '<create-note note-id="'+notes[prop]['id']+'" ';
              node += 'note-content = "'+notes[prop]['content']+ '" note-style="'+notes[prop]['style']+'" ';
              node += 'note-canvas = "'+canvas_id+'" ></create-note>';
          $(".canvas").append( $compile(node)($scope) );
        }
        break;
      }else{
          $(".canvas").html('');
        }
      }*/


    console.log('Load All Notes in this canvas_id: ' + $routeParams.canvas_id);
    $scope.dataLoading = true;

    NoteRest.notesList($routeParams.canvas_id, $scope.account.currentUser, function(response) {
          console.log('====NotesList====');
          if(response.success) {
              this.canvas_list_cookies = response.data;
              for (var i = this.canvas_list_cookies.length - 1; i >= 0; i--) {

                    var node =  '<create-note note-id="'+ this.canvas_list_cookies[i].id +'" ';
                        node += 'note-content = "'+this.canvas_list_cookies[i].content + '" ';
                        node += 'note-style="'+this.canvas_list_cookies[i].style+'" ';
                        node += 'note-canvas = "'+$routeParams.canvas_id+'" ></create-note>';
                    $(".canvas").append( $compile(node)($scope) );
                  }

          } else {
            console.log(response);
            $scope.error = response.error;
          }
          $scope.dataLoading = false;
    });

  });
