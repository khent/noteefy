AppControllers.controller('account', function ($scope, $rootScope, $routeParams, $cookies, $cookieStore, $http, $location, $filter, GoogleAppEngine, BeckRest, MainLoader){
    "use strict";


    $scope.account = {}; // model
    $scope.accountList = {}; // list



    $scope.loadList = function(){
        BeckRest.accountList()
            .success(function(data, status, headers, config){
                $scope.accountList = data.items;
            }).error(function(data, status, headers, config){
                $scope.accountList = {};
            });
    };
    $scope.loadList();

});
