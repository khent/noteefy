AppCore.config(['$routeProvider','$locationProvider', '$httpProvider',
    function($routeProvider, $locationProvider, $httpProvider) {

      $routeProvider.
        when('/ng-view/user', {
            controller: 'loginController',
            templateUrl: 'ng/templates/login/ui.html',
            hideMenus: true
        }).
        when('/ng-index', {
          controller: 'notesController',
          templateUrl: 'ng/templates/login/ui.html'
        }).




        when('/register', {
            controller: 'loginController',
            templateUrl: '/ngTemplates/login/register.html',
            hideMenus: true
        }).
         when('/ng-view/getStarted', {
            controller: 'getStartedController',
            templateUrl: 'ng/templates/home/index.html',
            hideMenus: true
        }).

        when('/', {
          controller: 'applicationController',
          templateUrl: 'ng/templates/notes/get-started.html'
        }).
        when('/canvas/:canvas_id', {
          controller: 'canvasController',
          templateUrl: 'ng/templates/notes/canvas.html'
        }).
        when('/logout', {
          controller : 'logoutController'
        }).



        otherwise({
            redirectTo: '/ng-view'
        });


  }]);
