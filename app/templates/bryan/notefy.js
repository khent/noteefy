(function(){
  var app = angular.module('notefyApp', ['ngRoute', 'ngSanitize','el-draggable']);

  app.config(['$routeProvider',function($routeProvider)
  {
    $routeProvider.
    when('/notes/notebooks', {
      templateUrl: 'templates/notes/newNotebooks.html',
      controller: 'notebooksController'}).

    when('/notes/createNotes', {
      templateUrl: 'templates/notes/newNotes.html',
      controller: 'notesController' }).

    when('/login', {
      templateUrl: 'templates/login/login.html',
      controller : 'loginController'}).
      otherwise({
      redirectTo: '/notes/notebooks'});
  }]);


app.controller('list',['$scope',function($scope){
  var uid = 1;

  $scope.contacts = [{id: 0,'name': 'Viral','email': 'hello@gmail.com','phone': '123-2343-44'}];

  $scope.saveContact = function () {

    if ($scope.newcontact.id == null) {
      $scope.newcontact.id = uid++;
      $scope.contacts.push($scope.newcontact);
    } else {

      for (i in $scope.contacts) {
        if ($scope.contacts[i].id == $scope.newcontact.id) {
          $scope.contacts[i] = $scope.newcontact;
        }
      }
    }
    $scope.newcontact = {};
  }


  $scope.delete = function (id) {

    for (i in $scope.contacts) {
      if ($scope.contacts[i].id == id) {
        $scope.contacts.splice(i, 1);
        $scope.newcontact = {};
      }
    }

  }


  $scope.edit = function (id) {
    for (i in $scope.contacts) {
      if ($scope.contacts[i].id == id) {
        $scope.newcontact = angular.copy($scope.contacts[i]);
      }
    }
  }
}
