from ferris.core import routing, plugins

# Routes all App handlers
routing.auto_route()

# Default root route
# routing.default_root()

# routing.redirect('/', to='/api/notefires/login')
routing.redirect('/', to='/ng-view')
# routing.redirect('/', to='/ng-view')

# Plugins
#plugins.enable('settings')
#plugins.enable('oauth_manager')
plugins.enable('angular')
