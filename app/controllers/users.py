from ferris import Controller, route, route_with, messages
from google.appengine.api import memcache
from google.appengine.ext import ndb
from app.models.user import User
from app.components.users import Users
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from app.components.firebase_behavior import FirebaseActions
from ferris.core.json_util import stringify as json_stringify
from ferris.core.template import render_template

import logging
import json
from app.components.utilities import check_json
from app.components.user_auth import allow_access
from ferris.core.template import render_template


class Users(Controller):
    class Meta:
            prefixes = ('api',)
            components = (Users,messages.Messaging,)
            Model = User
    @route
    def login(self):
        self.session['user'] = None
        return render_template("login/login.html")


    @route
    def signup(self):
        return render_template("login/register.html")




    @route
    def createuser(self):
        return self.components.users.createuser()


    @route
    def login_process(self):
        self.session = self.components.users.login_process()
        print '\nsession %s\n'%self.session
        if self.session:
            return self.redirect(self.uri(controller='notefires',action="homepage",user_email=self.session))
        else:
            return self.redirect(self.uri(action="login"))

    @route
    def api_list_users(self):
        if allow_access:
            self.context['data'] = json_stringify(User.list())
            print self.context['data']
            return self.context['data']
        else:
            return 404

    # @route
    # def api_list_users(self):
    #     print json_stringify(User.list())
    #     return json_stringify(User.list())


    @route
    def api_mylog(self):
        return render_template("users/login.html")

    @route
    def api_myreg(self):
        return render_template("users/signup.html")


    @route
    def api_mycreate(self):
        if allow_access(self):
            email = self.request.params['email']
            username = self.request.params['username']
            password = self.request.params['password']
            params = {'email': email, 'password': password}
            User.mycreate(params)
            return 200
        else:
            return 403

    @route
    def api_mylogin_process(self,ppuser,ppass):
        u = User.find_by_email(ppuser)
        if u:
            if u.password == ppass:
                print "LOG IN SUCCESSFUL"
                return 200
            else:
                return 403
        else:
            print "USER not exists"

            return 403


    @route
    def api_myregister_process(self,params):
        if allow_access(self):
            print type(self.request.body)
            data = check_json(self.request.body)
            print data
            User.create(data)
            return 200
        else:
            return 403
