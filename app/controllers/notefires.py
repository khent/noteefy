from ferris import Controller, route, route_with, messages
from google.appengine.api import memcache
from google.appengine.ext import ndb
from app.models.notefire import Notefire
from app.components.notefires import Notefires
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from app.components.firebase_behavior import FirebaseActions
from ferris.core.json_util import stringify as json_stringify
from ferris.core.template import render_template
from app.components.splitted_dt import TimeSplit
import logging
import json
from app.components.utilities import check_json
from app.components.user_auth import allow_access
from ferris.core.template import render_template


class Notefires(Controller):
    class Meta:
            prefixes = ('api',)
            components = (Notefires,messages.Messaging,)
            Model = Notefire

    @route_with(template= "/notefires/create_canvas/<canvasname>")
    def create_canvas(self,canvasname):
        return self.components.notefires.create_canvas(canvasname)


    @route
    def create_canvas_process(self,on,off):
        if allow_access(self):
            self.session["user_email"] = on
            dic = {}
            email = on
            canvasname = off
            data = memcache.get('mail_results')
            if data is None:
                print 'From Datastore'
                data = Notefire.list()
                memcache.add('mail_results', data, 60)
            else:
                print 'From Memcache'
            for item in data:
                if item.creator == email:
                    dic.update({item.canvas_name:item.creator})

            if dic:
                if canvasname in dic:
                    print True
                    return 403
                    # return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))
                else:
                    print False
                    params = {'creator': email,'canvas_name': canvasname,'notes':{}}
                    Notefire.create(params)
                    return 200
                    # return self.redirect(self.uri(action="canvas", canvasname=canvasname))
            else:
                print "no canvas yet"
                params = {'creator': email,'canvas_name': canvasname,'notes':{}}
                Notefire.create(params)
                return 200
                # return self.redirect(self.uri(action="canvas", canvasname=canvasname))
        else:
            return 403




    @route
    def canvas(self,canvasname):
        dic = {}
        try:
            if self.session['user_email']:
                print "user logged in"

            else:
                return 403
        except:
                return 403

        data = memcache.get('mail_results')
        canvases = []

        if data is None:
            source = 'From Datastore'
            data = Notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            source = 'From Memcache'
        for item in data:
               if item.creator == self.session['user_email']:
                   dic.update({item.canvas_name:item.key})


        if dic:
            if canvasname in dic:
                print True,canvasname
                for x, y in dic.items():
                    if str(canvasname) == x:
                        mem = y.get()
                        self.context['canvas_name'] = canvasname
                        self.context['clicked_canvas'] = mem.notes
                        self.context['last_edited'] = mem.modified
                        break
            else:
                print False
                self.context['canvas_name'] = canvasname
                self.context['clicked_canvas'] = {}
        # return 200
        # else:
        # #     return 500

    @route
    def update_note(self, canvas_name, note_id, note_content):
        dic = {}
        email = self.session['user_email']
        canvasname = canvas_name
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = Notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        for item in data:
               if item.creator == email:
                   dic.update({item.canvas_name:item.key})
        if dic:
            if canvas_name in dic:
                for x, y in dic.items():
                    if canvas_name == x:
                        mem = y.get()
                        break
        mem.notes.update({note_id : note_content})
        mem.put()
        return self.redirect(self.uri(action="canvas", canvasname=canvas_name))


    @route
    def update_canvas(self, canvas_name, note_content):
        dic = {}
        email = self.session['user_email']
        canvasname = canvas_name
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = Notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        for item in data:
               if item.creator == email:
                   dic.update({item.canvas_name:item.key})
        print dic
        if dic:
            if canvas_name in dic:
                print True,canvas_name
                for x, y in dic.items():
                    if canvas_name == x:
                        mem = y.get()
                        note_id = canvasname[0] + TimeSplit.dtnow()
                        mem.notes.update({note_id:note_content})
                        mem.put()
                        break
        return 200
        # return self.redirect(self.uri(action="canvas", canvasname=canvas_name))


    @route
    def delete_note(self, canvas_name, note_id):
        dic = {}
        email = self.session['user_email']
        canvasname = canvas_name
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = Notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        for item in data:
               if item.creator == email:
                   dic.update({item.canvas_name:item.key})
        if dic:
            if canvas_name in dic:
                for x, y in dic.items():
                    if canvas_name == x:
                        mem = y.get()
                        break
        del mem.notes[note_id]
        mem.put()
        return self.redirect(self.uri(action="canvas", canvasname=canvas_name))


    @route
    def delete_canvas(self, canvas_name):
        dic = {}
        email = self.session['user_email']
        canvasname = canvas_name
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = Notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        for item in data:
               if item.creator == email:
                   dic.update({item.canvas_name:item.key})
        print dic
        if dic:
            if canvas_name in dic:
                print True,canvas_name
                for x, y in dic.items():
                    if canvas_name == x:
                        Notefire.remove(y)
                        # note_id = canvasname[0] + TimeSplit.dtnow()
                        # mem.notes.update({note_id:note_content})
                        # mem.put()
                        break

        # Notefire.remove(mem)
        return 200
        # return self.redirect(self.uri(action="ng-view"))



    # @route_with(template= "/notefires/delete_canvas/<user_email>")
    # def delete_canvas(self,user_email):
    #     return self.components.notefires.delete_canvas(user_email)


    # @route
    # def delete_note(self):
    #     return self.components.notefires.delete_note()

    @route
    def homepage(self,user_email):
        # self.context['check_if_theres_canvas'] =
        # return render_template("notefires/homepage.html")
        return self.components.notefires.homepage(user_email)


    @route_with(template= "/notefires/cans/<cname>")
    def cans(self, cname):
        cname = "english"
        # return self.components.notefires.canvas(canvass)
        return self.redirect(self.uri(action="canvas",canvasname=cname))

    @route
    def api_list_canvas(self):
        # return Notefire.list()

        if allow_access(self):
            self.context['mydata'] = json_stringify(Notefire.list())
        else:
            return 403

    @route
    def api_canvaslists(self):
        return json_stringify(self.components.notefires.get_canvas_data())

    @route
    def api_userslists(self):
        return json_stringify(self.components.notefires.get_users_data())

    @route
    def api_createuser(self):
        if allow_access:
            return self.components.notefires.signup_process()
        else:
            return 400

    @route
    def api_login(self):
        self.components.notefires.login()
        return render_template("notefires/login.html")

    @route
    def api_home(self):#it process the log in details
        return self.components.notefires.home()

    @route
    def api_save_notes(self):
        return self.components.notefires.save_notes()

    @route
    def api_save_canvas(self):
        return self.components.notefires.save_canvas()


    @route_with(template='/api/notefires/delete_notes/<key>')
    def api_delete_notes(self,key):
        return self.components.notefires.delete_notes(key)



    @route
    def api_signup(self):
        return render_template("notefires/signup.html")

    @route
    def signup_process(self):
        return self.components.notefires.signup_process()


    @route_with(template="/api/frequencys/activate_mapping/<urlsafe_key>")
    def activate_mapping(self, urlsafe_key):
        # Get first the Active Mapping and delete to Firebase
        current_active_mapping = Notefire.get_active_mapping().get()
        print current_active_mapping
        # FirebaseActions.remove('FREQUENCY', current_active_mapping.key.urlsafe())

        # # Delete All to Datastore
        # Review.deactivate_all()
        # result = Review.activate_frequency_mapping(urlsafe_key)
        # if result:
        #     print self.util.stringify_json(True)
        #     return self.util.stringify_json(True)
        # print self.util.stringify_json(False)
        # return self.util.stringify_json(False)



    @route
    def ewaw(self):
        return self.components.notefires.ewaw()


    @route
    def open_canvas(self):
        return self.components.notefires.open_canvas()




