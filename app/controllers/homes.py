from ferris import Controller, route, route_with, messages
from google.appengine.api import memcache
from google.appengine.ext import ndb
from app.models.user import User
from app.components.users import Users
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from app.components.firebase_behavior import FirebaseActions
from ferris.core.json_util import stringify as json_stringify
from ferris.core.template import render_template

import logging
import json
from app.components.utilities import check_json
from app.components.user_auth import allow_access
from ferris.core.template import render_template


class Homes(Controller):
    class Meta:
            prefixes = ('api',)
            components = (Users,messages.Messaging,)
            Model =User

    @route
    def login(self):
        self.session['user'] = None
        return render_template("login/login.html")
