# from google.appengine.ext import ndb
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from ferris.core.template import render_template
from app.models.user import User
from ferris.core.json_util import stringify as json_stringify
from ferris.components.search import Search
import logging
import json
from app.components.firebase_behavior import FirebaseActions
import datetime
import time
from ferris.core.template import render_template

class Users(object):



    def __init__(self, controller):
        self.controller = controller
        self.user = User()



    def createuser(self):
        if self.controller.request.method == 'POST':
            email = self.controller.request.params['email']
            username = self.controller.request.params['username']
            password = self.controller.request.params['password']
            params = {'email': email, 'password': password}
            if self.user.create(params):
                return self.controller.redirect(self.controller.uri(action="login"))
            else:
                return self.controller.redirect(self.controller.uri(action="signup"))


    def login_process(self):
        if self.controller.request.method == 'POST':
            email = self.controller.request.params['email']
            password = self.controller.request.params['password']
            u = self.user.find_by_email(email)
            if u is None:
                # self.controller.context['email'] = email
                print "wala"
                self.controller.context['msg'] = 'Empty result'
            else:
                if str(u.password) == str(password):
                    self.controller.context['msg'] = 'LOG IN SUCCESSFULL!'
                    return email
#   return self.controller.redirect(self.controller.uri(controller='mails', action='index', user_email=email))
                else:
                    self.controller.context['msg'] = 'LOG IN FAIL!'
                    return



































    def session_isset(self):
        if self.controller.session['youser'] is None:
            return False
        else:
            return self.controller.session['youser']



    def sample(self):
        return 200

    def signup_process(self):
        email = self.controller.request.params['email']
        username = self.controller.request.params['username']
        password = self.controller.request.params['password']
        r = FirebaseActions.push("Users",{"email": email, "username": username, "password": password})
        return self.controller.redirect(self.controller.uri(action="login"))

    def save_canvas(self):
        for pars in self.controller.request.params:
            if pars == "txtAreaIdHide" or pars == "personal":
                pass
            else:
                print
                print self.controller.request.params[pars]
                print
                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                 self.controller.request.params["personal"],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(),"content":  self.controller.request.params[pars],
                                "id": pars
                                }})
        return self.controller.redirect(self.controller.uri(action='home',
        email=self.controller.session['youser'], password=self.controller.session['pw']))

    def save_notes(self):
        id_and_key = []
        id_only = []
        note_dates = []
        r_canvas = FirebaseActions.get("Canvas/")
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                r_canvas3 = FirebaseActions.get("Canvas/"+key+"/Notes/date/isoformat")
                if r_canvas3 is None:
                    pass
                else:
                    note_dates.append(r_canvas3)

        note_dates.sort()
        self.controller.context['getmydate'] = (note_dates[-1])


        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                id_and_key.append([data2['id'],key])
                id_only.append(data2['id'])

        for xid in id_only:
            for ywhole in id_and_key:
                if xid == ywhole[0]:
                    for parameter in self.controller.request.params:
                        if parameter == "save-notes" or parameter == "ok":
                            pass
                        else:
                            temp_parameter = parameter.split('AQUE')
                            if temp_parameter[0] in id_only:
                                print "update"
                                area_content1 = self.controller.request.params[parameter]
                                r = FirebaseActions.update("Canvas/"+temp_parameter[1]+"/Notes/",{"content": area_content1})
                            else:
                                print "push"
                                explodedcanvasname = str(self.controller.request.params['ok']).split("_")
                                area_content = self.controller.request.params[temp_parameter[0]]
                                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                explodedcanvasname[0],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(), "content": area_content, "id":temp_parameter[0]
                                }})
                # break the loop
                break
                print "break"
        return self.controller.redirect(self.controller.uri(action='home',
          email=self.controller.session['youser'], password=self.controller.session['pw']))



    def open_canvas(self):
        canvas_data = []
        r_canvas = FirebaseActions.get("Canvas/")
        exist = False
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                if data2['creator'] == str(self.controller.session['youser']):
                    canvas_data.append([data2['canvas-name'],data2['id']+'AQUE'+key,data2['content']])
                    exist = True
                    break
        if exist:
            return canvas_data


    def delete_notes(self,key):
        r = FirebaseActions.remove("Canvas/",key)
        return self.controller.redirect(self.controller.uri(action='home',
        email=self.controller.session['youser'], password=self.controller.session['pw']))


    def home(self):
        mylst = []
        ctr = 0
        r_users = FirebaseActions.get("Users/")
        email = self.controller.request.params['email']
        password = self.controller.request.params['password']
        exist = False

        for key, data in r_users.items():
            if str(data['email']) == email and str(data['password']) == password:
                exist =True
                break
        if exist:
            self.controller.session['youser'] = email
            self.controller.session['pw'] = password
            if self.controller.open_canvas():  # determines if the user already have a canvas
                for xxx in self.controller.open_canvas():
                    if xxx[0] in mylst:
                        print "appended already", xxx[0]
                    else:
                        print "not yet appended"
                        ctr = ctr + 1
                        mylst.append(xxx[0])
                print "mylist"
                print mylst

                #this context was used in rendering template
                #must be a json format/dictionary
                context = {"youser": email,"canvas_id":mylst,"canvas_whole": self.controller.open_canvas()}
                return render_template("users/home.html",context=context)
            else:
                print "\n\n No canvas yet!"
                context = {"youser": email}
                return render_template("users/home.html",context=context)
        else:
            return self.controller.redirect(self.controller.uri(action="login"))

