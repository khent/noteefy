# from google.appengine.ext import ndb
# from plugins.firebase_token_generator import create_token
# from plugins.firebase_rest import FirebaseRest
# from ferris.core.template import render_template
from app.models.notefire import Notefire
from app.models.user import User
# from ferris.core.json_util import stringify as json_stringify
# from ferris.components.search import Search
# import logging
# import json
# import time
from app.components.firebase_behavior import FirebaseActions
import datetime
from app.components.splitted_dt import TimeSplit
from ferris.core.template import render_template
from google.appengine.api import memcache

class Notefires(object):

    def __init__(self, controller):
        self.controller = controller
        self.notefire = Notefire()
        self.user = User()




    def canvas_checker(self, canvasname):
        dic = {}
        self.controller.session['user_email'] = "ken@h.k"
        email = self.controller.session['user_email']
        canvasname = canvasname
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    dic.update({item.canvas_name:item.creator})
        if dic:
            if canvasname in dic:
                print True
                return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))
            else:
                print False
                params = {'creator': email,'canvas_name': canvasname,'notes':{}}
                self.notefire.create(params)
                return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))
        else:
            print "walang canvas"
            params = {'creator': email,'canvas_name': canvasname,'notes':{}}
            self.notefire.create(params)
            return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))


    def create_canvas(self, canvasname):
        dic = {}
        self.controller.session['user_email'] = "ken@h.k"
        email = self.controller.session['user_email']
        canvasname = canvasname
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    dic.update({item.canvas_name:item.creator})
        if dic:
            if canvasname in dic:
                print True
                return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))
            else:
                print False
                params = {'creator': email,'canvas_name': canvasname,'notes':{}}
                self.notefire.create(params)
                return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))
        else:
            print "walang canvas"
            params = {'creator': email,'canvas_name': canvasname,'notes':{}}
            self.notefire.create(params)
            return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname))

        return 200


        # if check_canvas:
        #     dic = check_canvas.notes
        #     print "yes"
        #     return 200
        #     # check_canvas.notes.update({"bagonote":"bagoA2"})
        #     # check_canvas.put()
        # else:
        #     print "nope"
        #     params = {'creator': email,'canvas_name': canvasname,'notes':{}}
        #     if self.notefire.create(params):
        #         return self.controller.redirect(self.controller.uri(action="homepage", user_email=email))

        # return 200

        # if canvasname in canvases:
        #     print "yes again",canvasname

        #     # params = {'canvas_name': canvasname}
        #     # self.notefire.update(params)
        #     uuu = self.notefire.find_by_canvas_name(canvasname)
        #     if uuu is None:
        #         print "NONEEEEEEEEE"
        #     else:
        #         print "YESSSSSSSSSS"
        #     return 200
        # else:
        #     print "no again",canvasName
        #     params = {'creator': email,'canvas_name': canvasname,'notes':{'engnote1':'engA','engnote2':'engB'}}
        #     if self.notefire.create(params):
        #         # return 200
        #         return self.controller.redirect(self.controller.uri(action="homepage", user_email=email))
        #     else:
        #         return 404
        #         # return self.controller.redirect(self.controller.uri(action="signup"))


    def delete_canvas(self, email):
        dic = {}
        email = self.controller.session['user_email']
        canvasName = self.controller.request.body.split('=')
        canvasname = canvasName[1]
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    dic.update({item.canvas_name:item.key})
        if dic:
            if canvasname in dic:
                print True
                for x, y in dic.items():
                    if canvasname == x:
                        print "<<", x , y
                        self.notefire.remove(y)
                        return self.controller.redirect(self.controller.uri(action="homepage", user_email=email))
                        break
            else:
                print False
                return 404
        return 200


    def update_canvas(self, canvas_name):
        dic = {}
        email = self.controller.session['user_email']
        canvasName = self.controller.request.body.split('=')
        canvasname = canvasName[1].split('&')
        print email,canvasname[0]
        note_content = self.controller.request.params[canvasname[1]]
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    dic.update({item.canvas_name:item.creator})

        if dic:
            if canvasname[0] in dic:
                print True
                check_canvas = self.notefire.find_by_creator(email)
                print check_canvas.notes
                note_id = canvasname[0] + TimeSplit.dtnow()
                check_canvas.notes.update({note_id:note_content})
                check_canvas.put()
            else:
                print False
        else:
            print "wala pa"
            return 500
        return 200
        # print
        # note_id = canvasname[0] + TimeSplit.dtnow()
        # check_canvas.notes.update({note_id:note_content})
        # check_canvas.put()
        # print canvasname[1]
        # print check_canvas.notes

        # import time
        # time.sleep(1)
        # return self.controller.redirect(self.controller.uri(action="canvas", canvasname=canvasname[0]))









    def canvas(self,canvasname):
        try:
            if self.controller.session['user_email']:
                print "user logged in"
            else:
                return 403
        except:
                return 403
        if canvasname == "<canvasname>":
            print "clicked"
            canvasName = self.controller.request.body.split('=')
            canvasname = canvasName[0]
        else:
            print 'searched'


        data = memcache.get('mail_results')
        canvases = []
        if data is None:
            source = 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            source = 'From Memcache'

        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    # canvases_name.append(item)
                    if item.canvas_name == canvasname:
                        canvases.append(item)
        self.controller.context['canvas_name'] = canvasname
        check_canvas = self.notefire.find_by_creator(self.controller.session['user_email'])
        print check_canvas
        if check_canvas:
            self.controller.context['clicked_canvas'] =check_canvas.notes



    def delete_note(self):
        note_id = self.controller.request.params["note_name_delete"]
        dic = {}
        email = self.controller.session['user_email']
        canvasName = self.controller.request.body.split('=')
        canvasname = canvasName[1].split('&')
        print email,canvasname[0]
        note_content = self.controller.request.params[canvasname[1]]
        data = memcache.get('mail_results')
        if data is None:
            print 'From Datastore'
            data = self.notefire.list()
            memcache.add('mail_results', data, 60)
        else:
            print 'From Memcache'
        if self.controller.request.method=='POST':
            for item in data:
                if item.creator == self.controller.session['user_email']:
                    dic.update({item.canvas_name:item.creator})
        counter = 0
        if dic:
            if canvasname[0] in dic:
                print True
                check_canvas = self.notefire.find_by_creator(email)
                # print check_canvas.notes
                for key in check_canvas.notes:
                    if note_id == key:
                        counter = counter + 1
                        del check_canvas.notes[note_id]
                        check_canvas.put()
                        break
                print counter, " same key/s found"
            else:
                print False
        else:
            print "wala pa"
            return 500
        return 200

    def homepage(self, user_email):
        if self.user.find_by_email(user_email):
            print "user exists"
            self.controller.session['user_email'] = user_email
            data = memcache.get('mail_results')
            canvases = []
            if data is None:
                source = 'From Datastore'
                data = self.notefire.list()
                memcache.add('mail_results', data, 60)
            else:
                source = 'From Memcache'

            if self.controller.request.method=='POST':
                print "no"
                canvasName = self.controller.request.body.split('=')
                subject = canvasName[0]
                print subject
                print "8888888888888888888888"
                # for item in data:
                #     if item.canvas_name == subject and item.creator == user_email:
                #         canvases.append(item)



            else:
                for item in data:
                    if item.creator == user_email:
                        # canvases_name.append(item)
                        canvases.append(item)


            self.controller.session['user_email'] = user_email
            self.controller.context['user_email'] = user_email
            self.controller.context['source']  = source


            self.controller.context['canvas_name'] = canvases
        else:
            print "user not exists"
            return self.controller.redirect(self.controller.uri(controller='users', action='login'))
















    # def login_process(self):
    #     if self.controller.request.method == 'POST':
    #         email = self.controller.request.params['email']
    #         password = self.controller.request.params['password']
    #         u = self.notefire.find_by_email(email)
    #         if u is None:
    #             # self.controller.context['email'] = email
    #             print "wala"
    #             self.controller.context['msg'] = 'Empty result'
    #         else:
    #             if str(u.password) == str(password):
    #                 self.controller.context['msg'] = 'LOG IN SUCCESSFULL!'
    #                 return email
    #             #     # return self.controller.redirect(self.controller.uri(controller='mails', action='index', user_email=email))
    #             else:
    #                 self.controller.context['msg'] = 'LOG IN FAIL!'
    #                 return




































    def session_isset(self):
        if self.controller.session['youser'] is None:
            return False
        else:
            return self.controller.session['youser']



    def sample(self):
        return 200

    def signup_process(self):
        email = self.controller.request.params['email']
        username = self.controller.request.params['username']
        password = self.controller.request.params['password']
        r = FirebaseActions.push("Users",{"email": email, "username": username, "password": password})
        return self.controller.redirect(self.controller.uri(action="login"))

    def save_canvas(self):
        for pars in self.controller.request.params:
            if pars == "txtAreaIdHide" or pars == "personal":
                pass
            else:
                print
                print self.controller.request.params[pars]
                print
                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                 self.controller.request.params["personal"],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(),"content":  self.controller.request.params[pars],
                                "id": pars
                                }})
        return self.controller.redirect(self.controller.uri(action='home',
        email=self.controller.session['youser'], password=self.controller.session['pw']))

    def save_notes(self):
        id_and_key = []
        id_only = []
        note_dates = []
        r_canvas = FirebaseActions.get("Canvas/")
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                r_canvas3 = FirebaseActions.get("Canvas/"+key+"/Notes/date/isoformat")
                if r_canvas3 is None:
                    pass
                else:
                    note_dates.append(r_canvas3)

        note_dates.sort()
        self.controller.context['getmydate'] = (note_dates[-1])


        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                id_and_key.append([data2['id'],key])
                id_only.append(data2['id'])

        for xid in id_only:
            for ywhole in id_and_key:
                if xid == ywhole[0]:
                    for parameter in self.controller.request.params:
                        if parameter == "save-notes" or parameter == "ok":
                            pass
                        else:
                            temp_parameter = parameter.split('AQUE')
                            if temp_parameter[0] in id_only:
                                print "update"
                                area_content1 = self.controller.request.params[parameter]
                                r = FirebaseActions.update("Canvas/"+temp_parameter[1]+"/Notes/",{"content": area_content1})
                            else:
                                print "push"
                                explodedcanvasname = str(self.controller.request.params['ok']).split("_")
                                area_content = self.controller.request.params[temp_parameter[0]]
                                r = FirebaseActions.push("Canvas/",{"Notes":{"canvas-name":
                                explodedcanvasname[0],
                                "creator":self.controller.session['youser'],
                                "date" : datetime.datetime.now(), "content": area_content, "id":temp_parameter[0]
                                }})
                # break the loop
                break
                print "break"
        return self.controller.redirect(self.controller.uri(action='home',
          email=self.controller.session['youser'], password=self.controller.session['pw']))



    def open_canvas(self):
        canvas_data = []
        r_canvas = FirebaseActions.get("Canvas/")
        exist = False
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                if data2['creator'] == str(self.controller.session['youser']):
                    canvas_data.append([data2['canvas-name'],data2['id']+'AQUE'+key,data2['content']])
                    exist = True
                    break
        if exist:
            return canvas_data


    def delete_notes(self,key):
        r = FirebaseActions.remove("Canvas/",key)
        return self.controller.redirect(self.controller.uri(action='home',
        email=self.controller.session['youser'], password=self.controller.session['pw']))


    def home(self):
        mylst = []
        ctr = 0
        r_users = FirebaseActions.get("Users/")
        email = self.controller.request.params['email']
        password = self.controller.request.params['password']
        exist = False

        for key, data in r_users.items():
            if str(data['email']) == email and str(data['password']) == password:
                exist =True
                break
        if exist:
            self.controller.session['youser'] = email
            self.controller.session['pw'] = password
            if self.controller.open_canvas():  # determines if the user already have a canvas
                for xxx in self.controller.open_canvas():
                    if xxx[0] in mylst:
                        print "appended already", xxx[0]
                    else:
                        print "not yet appended"
                        ctr = ctr + 1
                        mylst.append(xxx[0])
                print "mylist"
                print mylst

                #this context was used in rendering template
                #must be a json format/dictionary
                context = {"youser": email,"canvas_id":mylst,"canvas_whole": self.controller.open_canvas()}
                return render_template("notefires/home.html",context=context)
            else:
                print "\n\n No canvas yet!"
                context = {"youser": email}
                return render_template("notefires/home.html",context=context)
        else:
            return self.controller.redirect(self.controller.uri(action="login"))


    def ewaw(self):
        return "ewaw"
        # # params = {'email': "kenneth@h.k", 'password': "12345"}
        # # self.notefire.create(params)
        # # current_active_mapping = Notefire.semail("kenneth@h.k")
        # # print current_active_mapping
        # email = "jona@h.k"
        # password = "2"
        # f = FirebaseRest("Users/")
        # r = f.get()
        # json_encoded = json.dumps(r)
        # decoded_data = json.loads(json_encoded)
        # player  = None
        # exist = False

        # for key, data in decoded_data.items():
        #     if str(data['email']) == email and str(data['password']) == password:
        #         exist =True
        #         player = data
        #         break
        # if exist:
        #     print "okay"
        #     #------------UPDATE VIA BRUTE FORCE
        #     # FirebaseActions.remove('Users/', key)
        #     # r = f.push(json_stringify({"email": "email", "username": "username", "password": "password"}))
        #     #-----------UPDATE via FIREBASE_REST
        #     # ff = FirebaseRest("Users/"+key)
        #     # ff.update(json_stringify({"content":"tr"}))
        #     # self.controller.context['my_data'] = data['username']
        #     return
        # else:
        #     print "not okay"


    def get_latest_saved_date(self):
        note_dates = []
        r_canvas = FirebaseActions.get("Canvas/")
        for key, data in r_canvas.items():
            r_canvas2 = FirebaseActions.get("Canvas/"+key)
            for key2, data2 in r_canvas2.items():
                r_canvas3 = FirebaseActions.get("Canvas/"+key+"/Notes/date/isoformat")
                if r_canvas3 is None:
                    pass
                else:
                    note_dates.append(r_canvas3)
        note_dates.sort()
        self.controller.context['getmydate'] = (note_dates[-1])
        return self.controller.context['getmydate']





    def get_canvas_data(self):
        r_canvas = FirebaseActions.get("Canvas/")
        return r_canvas



    def get_users_data(self):
        r_canvas = FirebaseActions.get("Users/")
        return r_canvas
