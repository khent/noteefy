import datetime

class TimeSplit:
    @classmethod
    def dtnow(self):
        iii = (datetime.datetime.now().isoformat())
        iia = "".join(iii.split("-")).split(":")
        iib = "".join(iia).split(".")
        iic = "".join(iib)

        return iic
