from google.appengine.api import app_identity
"""
This file is used to configure application settings.

Do not import this file directly.

You can use the settings API via:

    from ferris import settings

    mysettings = settings.get("mysettings")

The settings API will load the "settings" dictionary from this file. Anything else
will be ignored.

Optionally, you may enable the dynamic settings plugin at the bottom of this file.
"""

settings = {}

settings['timezone'] = {
    'local': 'US/Eastern'
    # 'local': 'Asia/Manila'
}

settings['email'] = {
    # Configures what address is in the sender field by default.
    'sender': 'notifications@' + str(app_identity.get_application_id()) + '.appspotmail.com'
}

settings['app_config'] = {
    'webapp2_extras.sessions': {
        # WebApp2 encrypted cookie key
        # You can use a UUID generator like http://www.famkruithof.net/uuid/uuidgen
        'secret_key': '9a788030-837b-11e1-b0c4-0800200c9a66',
    }
}

settings['oauth2'] = {
    # OAuth2 Configuration should be generated from
    # https://code.google.com/apis/console
    'client_id': None,  # XXXXXXXXXXXXXXX.apps.googleusercontent.com
    'client_secret': None
}

# Enables or disables app stats.
# Note that appstats must also be enabled in app.yaml.
settings['appstats'] = {
    'enabled': False,
    'enabled_live': False
}

settings['twilio'] = {
    'account_sid': "AC379e0e20e93d2108b38be445e2e6bb77",
    'auth_token': "0c030ab8dbaa2483a2c397ab694a4d5e"
}

settings['gcm'] = {

    'DEV': {
        'public_api_key': 'AIzaSyAcqgMzvHSvo2AWLCG_cIilL_0zzUgOeL0',
        'project_number': '1032404959078'
    },

    'QA': {
        'public_api_key': 'AIzaSyAcqgMzvHSvo2AWLCG_cIilL_0zzUgOeL0',
        'project_number': '1032404959078'
    },

    'PROD': {
        'public_api_key': 'AIzaSyDO1l0E0cU2MSqL3Qrtj562u2Lou1s3tbk',
        'project_number': '515445490988'
    }

}

settings['console'] = {
    'public_api': 'AIzaSyAcqgMzvHSvo2AWLCG_cIilL_0zzUgOeL0'
}

settings['firebase'] = {
    # Christain local
    # 'local': {"url": "https://blazing-inferno-4388.firebaseio.com/", "secret": "U1BVNuplcOXRaGZAuNMzBKQYE6xGQaSVv6kZGgg3"},

    # Ray local
    #'local': {"url": "https://becktaxi-ray-local.firebaseio.com/", "secret": "JPbr9vYLjmuV6YcwW2Rf8xxl7cSbgyBrFls7CJWm"},

    # Dee-Jay local
    'local': {"url": "https://notefy.firebaseio.com/", "secret": "vRcbODnL0xAp6yjpNkXkGj4DlnCpYgkp8SSAwocN"},

    # Eric local
    # 'local': {"url": "https://burning-torch-404.firebaseio.com/", "secret": "LqLHDpt3p8qxn8J4LYdy7PzxrLGnGbUljCJBhlXt"},

    # John local
    # 'local': {"url": "https://becktaxi-john-local.firebaseio.com/", "secret": "rZjjbHKddsNqZ9dWSpNeVTObco5G0yC1piDvVDR0"},

    # appengine environment
    'DEV': {"url": "https://notefy.firebaseio.com/", "secret": "vRcbODnL0xAp6yjpNkXkGj4DlnCpYgkp8SSAwocN"},
    'QA': {"url": "https://notefy.firebaseio.com/", "secret": "vRcbODnL0xAp6yjpNkXkGj4DlnCpYgkp8SSAwocN"},
    'PROD': {"url": "https://notefy.firebaseio.com/", "secret": "vRcbODnL0xAp6yjpNkXkGj4DlnCpYgkp8SSAwocN"}
}

settings['google_analytics'] = {
    'trackcode_DEV': 'UA-54948720-1',   # DEV
    'trackcode_QA': 'UA-55280566-1',    # QA
    'trackcode_PROD': 'UA-55277969-1'   # PROD
}

settings['braintree'] = {
    'DEV': {
        'merchant_id': 'zpzzwtjrj5bgq3kb',
        'public_key': 'zm5k95hbh6tbjqbh',
        'private_key': '67a8902004c12393acf079c637577e74'
    },
    'QA': {
        'merchant_id': '000',
        'public_key': '456',
        'private_key': '789'
    },
    'PROD': {
        'merchant_id': '000',
        'public_key': '456',
        'private_key': '789'
    }
}

# Special override testing account for automated/performance tests.
# Used on the dev and QA servers only.
settings['testing_override'] = {
    'token': 'f2594823c964e093c22ddd98bcfb7b5028da97e528dfd871d66c0a6e950274ef',
    'email': 'testing_override_superadmin@sherpademo.com'
}

settings['allowed_ip'] = (
    # localhost
    '127.0.0.1',

    # GDC office
    '125.252.68.90'
    '125.252.68.91'
    '125.252.68.92'
    '125.252.68.93',
    '125.252.68.94',

    # ???
    '180.191.217.46',

    # ???
    '192.168.2.104',

    # rachel
    '121.54.54.62',
    '121.54.54.38',

    # ray
    '111.235.89.61'
)

settings['upload'] = {
    'use_cloud_storage': False
}
# Optionally, you may use the settings plugin to dynamically
# configure your settings via the admin interface. Be sure to
# also enable the plugin via app/routes.py.

# import plugins.settings

# import any additional dynamic settings classes here.

# import plugins.service_account.settings

# Un-comment to enable dynamic settings

# plugins.settings.activate(settings)
